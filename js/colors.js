/**
 * @author Daniel Goberitz <dalgo86@gmail.com>
 * @overview Widget que se encarga de mostrar una interfaz de usuario para 
 *   cambiar los temas de colores.
 *
 */
(function($){

	window.Themes = {
		themes:{},
		file: 'css/colors.css',
		actualTheme: null,
		
		init: function(){
			this.readFile();
			this.element = $('#ThemeChanger');
		},
		
		readFile: function(){
			var me = this;
			$.ajax({
				url: 'css/colors.css',
				success: function(data){
					me.parseFile(data);
					me.fillThemes();
					me.restoreLastColor();
				},
				dataType: 'text'
			});
		},
		
		parseFile: function(source){
			source = source.replace(/[\r\n\t]/g, '');
			var matches = source.match(/body\.([\w]*){[^}]*}/g),
				i,
				strip,
				b = $('body')
			;
			console.log(matches);
			for(i in matches){
				if(matches.hasOwnProperty(i)){
					console.log(matches[i]);
					strip = matches[i].match(/body\.([\w]*){[\s]*[\w]*:([^;]*).*;}/);
					
					console.log(strip);
					
					this.themes[ strip[1] ] = strip[2];
					
					if(b.hasClass(strip[1])){
						this.actualTheme = strip[1];
					}
				}
			}
			
			console.log(this.themes);
		},
		
		fillThemes: function(){
			var i,
				me = this
			;
			for(i in this.themes){
				if(this.themes.hasOwnProperty(i)){
					(function(name, color){
						$('<li>')
							.attr('title', name)
							.css('background', color)
							.click(function(){
								me.changeTheme(name);
							})
							.appendTo(me.element)
						;
					}(i, this.themes[i]));
					
				}
			}
		},
		
		changeTheme: function(theme){
			$('body').removeClass(this.actualTheme).addClass(theme);
			this.actualTheme = theme;
			window.localStorage.setItem('theme', theme);
		},
		
		restoreLastColor: function(){
			var lastTheme = window.localStorage.getItem('theme');
			if(lastTheme){
				this.changeTheme(lastTheme);
			}
		}
	
	};



	$(document).ready(function(){
		window.Themes.init();
	});

}(jQuery))